﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemySpawner : MonoBehaviour {

    public GameObject EnemyPrefab;

    public float minX = -47.0f;
    public float maxX = 47.0f;

    public float minY = -26.0f;
    public float maxY = 26.0f;

    public int NumberofEnemyAIAtRuntime = 5;

    public int AICap = 10;

    public Text number;

    // Use this for initialization
    void Awake () {
        for (int i = 0; i < NumberofEnemyAIAtRuntime; i++)
        {
            Spawn(EnemyPrefab);
        }
    }
	
	// Update is called once per frame
	void Update () {
        
        while (CheckNumberofAI() < AICap)
        {
            Spawn(EnemyPrefab);
        }

        number.text = "Enemies : " + CheckNumberofAI().ToString();
    }

    void Spawn(GameObject Prefab)
    {
        Vector3 SpawnPoint = new Vector3(Random.Range(minX, maxX), Random.Range(minY, maxY), 0.0f);
        Instantiate(Prefab, SpawnPoint, Quaternion.identity);
    }

    int CheckNumberofAI()
    {
        GameObject[] gam = GameObject.FindGameObjectsWithTag("Enemy");
        return gam.Length;
    }
}
