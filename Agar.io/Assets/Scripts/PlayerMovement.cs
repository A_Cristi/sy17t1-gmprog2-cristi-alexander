﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public Camera Camera;
    public float Speed = 15.0f;

    public Vector2 Size = new Vector3(1.0f, 1.0f);

    private float Distance;

    // Use this for initialization
    void Start () {
        transform.localScale = Size;
    }
	
	// Update is called once per frame
	void Update () {
        Vector2 target = Camera.ScreenToWorldPoint(Input.mousePosition);        
        transform.position = Vector3.MoveTowards(transform.position, target, Speed * Time.deltaTime / transform.localScale.x);
    }
}
