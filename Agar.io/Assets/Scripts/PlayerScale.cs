﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScale : MonoBehaviour {
    public float Scale;

    private Vector3 scaleToAdd;
    private Vector3 Minimum;
	// Use this for initialization
	void Start () {

        scaleToAdd = new Vector3(Scale, Scale, 0);
        Minimum = new Vector3(1, 1, 0);
	}
	
	// Update is called once per frame
	void Update () {
        
        if (Input.GetKeyDown(KeyCode.KeypadPlus)) transform.localScale += scaleToAdd;
        if (Input.GetKeyDown(KeyCode.KeypadMinus)) transform.localScale -= scaleToAdd;

    }
}
