﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraClamp : MonoBehaviour {
	
	// Update is called once per frame
	void Update () {

        Vector3 cameralock = transform.position;
        cameralock.z = Mathf.Clamp(transform.position.z, -15.0f, -15.1f);
        transform.position = cameralock;
    }
}
