﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FoodSpawner : MonoBehaviour {

    
    public GameObject FoodPrefab;

    public float minX = -47.0f;
    public float maxX = 47.0f;

    public float minY = -26.0f;
    public float maxY = 26.0f;

    public int NumberofFoodAtRuntime = 30;     

    public int FoodCap = 100;

    public Text number;
    void Awake()
    {
        for (int i = 0; i < NumberofFoodAtRuntime; i++)
        {
            Spawn(FoodPrefab);
        }
    }

    void Update()
    {
        while (CheckNumberofFood() < FoodCap)
         {
             Spawn(FoodPrefab);
         }

        number.text = "Food : " + CheckNumberofFood().ToString();
    }

    void Spawn(GameObject Prefab)
    {
        Vector3 SpawnPoint = new Vector3(Random.Range(minX, maxX), Random.Range(minY, maxY), 0.0f );
        Instantiate(Prefab, SpawnPoint, Quaternion.identity);
    }

    int CheckNumberofFood()
    {
        GameObject[] gam = GameObject.FindGameObjectsWithTag("Food");
        return gam.Length;
    }
}