﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerClamp : MonoBehaviour {
    	
	// Update is called once per frame
	void Update () {
        Vector3 PlayerLock = transform.position;
        PlayerLock.x = Mathf.Clamp(transform.position.x, -47.0f, 47.0f);
        PlayerLock.y = Mathf.Clamp(transform.position.y, -26.0f, 26.0f);
        PlayerLock.z = 0.0f;
        transform.position = PlayerLock;
    }
}
