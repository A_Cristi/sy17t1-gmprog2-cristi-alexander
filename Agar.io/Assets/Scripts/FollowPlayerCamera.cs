﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayerCamera : MonoBehaviour {

    public GameObject target;
    public float speed = 5.0f;

    private float Distance;

    private float Scale;

    private float CamOrthSize;

    // Use this for initialization
    void Start () {
        CamOrthSize = 7;
    }
	
	// Update is called once per frame
	void Update () {
        transform.position = Vector3.MoveTowards(transform.position, target.transform.position, speed * Time.deltaTime);
        Scale = target.GetComponent<PlayerScale>().Scale;
        Camera.main.orthographicSize = Mathf.Lerp(CamOrthSize, Scale, Time.deltaTime * 5);
    }
}
    