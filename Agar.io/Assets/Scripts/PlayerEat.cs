﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerEat : MonoBehaviour {

    public Vector3 FoodSize = new Vector3(0.2f, 0.2f, 0.2f);

    private int score = 0;

    public Text ScoreText;    
    public float Scale;

    // Use this for initialization
    void Start () {
        ScoreText.text = "Score : " + score.ToString();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter2D(Collision2D other)
    {
        //Debug.Log("Collided");

        if (other.gameObject.tag == "Food")
        {
            Destroy(other.gameObject);
            transform.localScale += FoodSize;
            score++;
            ScoreText.text = "Score : " + score.ToString();            
        }

        if (other.gameObject.tag == "Enemy")
        {

            if (FindClosestObj("Enemy").transform.localScale.x > transform.localScale.x)
            {
                Destroy(this.gameObject);
            }

            else
            {
                transform.localScale += FindClosestObj("Enemy").transform.localScale;
                Destroy(FindClosestObj("Enemy"));
                Scale += FindClosestObj("Enemy").transform.localScale.x;

                score += 10;
            }
        }
    }


    GameObject FindClosestObj(string Tag)
    {
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag(Tag);
        GameObject closest = null;

        float distance = Mathf.Infinity;
        //float distance = 10.0f;
        Vector3 position = transform.position;
        foreach (GameObject go in gos)
        {
            Vector3 diff = go.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                closest = go;
                distance = curDistance;
            }
        }
        return closest;
    }
}
