﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIState : MonoBehaviour {

    enum State {

        FindFood,
        RunFromPlayer,
        RunFromAI,
        ChasePlayer,
        ChaseAI
    }

    public Vector3 Size = new Vector3(1.0f, 1.0f, 1.0f);

    float timeSinceLastChange = 0.0f;
    State curState;

    float distanceToFood, distanceToPlayer, distanceToOtherAI;

    public float Speed = 3.0f;

    // Use this for initialization
    void Start () {
        SetState(State.FindFood);

        transform.localScale = Size;
    }
	
	// Update is called once per frame
	void Update () {
        CalculateDistances();
        AssignState();
        Action(curState);
    }

    void CalculateDistances()
    {
        distanceToFood = Vector3.Distance(transform.position, FindClosestObj("Food").transform.position);
        distanceToPlayer = Vector3.Distance(transform.position, FindClosestObj("Player").transform.position);
        distanceToOtherAI = Vector3.Distance(transform.position, FindClosestObj("Enemy").transform.position);

    }

    void Action(State st)
    {
        switch (st)
        {
            case State.ChaseAI:
                transform.position = Vector3.MoveTowards(transform.position, FindClosestObj("Enemy").transform.position, Speed * Time.deltaTime / transform.localScale.x);
                break;

            case State.ChasePlayer:
                transform.position = Vector3.MoveTowards(transform.position, FindClosestObj("Player").transform.position, Speed * Time.deltaTime / transform.localScale.x);
                break;

            case State.FindFood:
                transform.position = Vector3.MoveTowards(transform.position, FindClosestObj("Food").transform.position, Speed * Time.deltaTime / transform.localScale.x);
                break;

            case State.RunFromPlayer:
                transform.position = Vector3.MoveTowards(transform.position, FindClosestObj("Player").transform.position, -Speed * Time.deltaTime / transform.localScale.x);
                break;

            case State.RunFromAI:
                transform.position = Vector3.MoveTowards(transform.position, FindClosestObj("Enemy").transform.position, -Speed * Time.deltaTime / transform.localScale.x);
                break;

        }

    }

    void SetState(State st)
    {
        curState = st;
        timeSinceLastChange = Time.time;
    }
    GameObject FindClosestObj(string tag)
    {
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag(tag);
        GameObject closest = null;

        float distance = Mathf.Infinity;
        //float distance = 10.0f;
        Vector3 position = transform.position;
        foreach (GameObject go in gos)
        {
            Vector3 diff = go.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                closest = go;
                distance = curDistance;
            }
        }
        return closest;
    }

    void AssignState()
    {
        if ((distanceToOtherAI < distanceToPlayer || distanceToOtherAI < distanceToFood)
             && transform.localScale.x > FindClosestObj("Enemy").transform.localScale.x)

            SetState(State.ChaseAI);


        else if ((distanceToPlayer < distanceToOtherAI || distanceToPlayer < distanceToFood)
            && transform.localScale.x > FindClosestObj("Player").transform.localScale.x)

            SetState(State.ChasePlayer);


        else if ((distanceToPlayer < distanceToOtherAI || distanceToPlayer < distanceToFood)
            && transform.localScale.x < FindClosestObj("Player").transform.localScale.x)

            SetState(State.RunFromPlayer);

        else if ((distanceToPlayer < distanceToOtherAI || distanceToPlayer < distanceToFood)
            && transform.localScale.x < FindClosestObj("Enemy").transform.localScale.x)

            SetState(State.RunFromAI);

        else
            SetState(State.FindFood);
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Food")
        {
            Destroy(other.gameObject);

            transform.localScale += Size;
        }

        if (other.gameObject.tag == "Enemy")
        {
            if (other.transform.localScale.x < transform.localScale.x)
            {
                Destroy(other.gameObject);
                transform.localScale += Size;
            }

            else
            {
                Destroy(this);
            }

        }

    }
}
