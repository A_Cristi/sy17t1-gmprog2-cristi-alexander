﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Goal : MonoBehaviour {
    public Text HPText;

    public int HP;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        HPText.text = "HP - " + HP.ToString();
        if (HP <= 0)
        {
            Application.Quit();
            //SceneManager.LoadScene("Over");
        }
    }
}
