﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Base : MonoBehaviour {

    private bool HasBuilding;

    public GoldHolder GH;
    public Shop SH;

    public Text TowerOnMeTXT;
    private GameObject TowerOnMe;

    public Text TowerUpgradeCost;
    public Text TowerLvl;
    // Use this for initialization
    void Start () {
        HasBuilding = false;
    }
	
	// Update is called once per frame
	void Update () {
        if(TowerOnMe != null)
        {
            TowerOnMeTXT.text = TowerOnMe.name;            
        }
                  

    }

    void OnMouseUpAsButton()
    {
        if (SH.Selected != null)
        {
            if (HasBuilding == false && GH.gold >= SH.Selected.GetComponent<TowerBase>().twrCost)
            {
                HasBuilding = true;                
                Build();
            }
        }
    }

    void Build()
    {
        TowerOnMe = GameObject.Instantiate(SH.Selected, transform.position, Quaternion.identity);       
        GH.gold -= SH.Selected.GetComponent<TowerBase>().twrCost;
    }

    
}
