﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{

    public GameObject ArrowTower;
    public GameObject CannonTower;
    public GameObject IceTower;
    public GameObject FireTower;

    public GameObject Selected;
    public Text TowerDisplay;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void SelectArrow()
    {
        Selected = ArrowTower;

        TowerDisplay.text = "Arrow Tower";
    }

    public void SelectCannon()
    {
        Selected = CannonTower;
        TowerDisplay.text = "Cannon Tower";
    }

    public void SelectIce()
    {
        Selected = IceTower;
        TowerDisplay.text = "Ice Tower";
    }

    public void SelectFire()
    {
        Selected = FireTower;
        TowerDisplay.text = "Fire Tower";
    }

}
