﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public GameObject Target;

    protected float damage;

    public float Lowdamage;
    public float Highdamage;

    public float speed;

    float distance;
	// Use this for initialization
	protected virtual void Start () {
        AssignDam();
    }

    protected void AssignDam()
    {
        damage = Random.Range(Lowdamage, Highdamage);
        Debug.Log(damage);
    }

    // Update is called once per frame
    public virtual void Update () {
       


        if (Target == null)
        {
            Destroy(gameObject);
            return;
        }
        distance = Vector3.Distance(Target.transform.position, transform.position);


        //if (transform.position != Target.transform.position && Target != null)
        //{
        //    transform.position = Vector3.MoveTowards(transform.position, Target.transform.position, speed * Time.deltaTime);
        //}

        if (distance > .5f)
        {
            transform.position = Vector3.MoveTowards(transform.position, Target.transform.position, speed * Time.deltaTime);
        }

        else
        {
            Debug.Log("damage");
            Hit();
            
        }
    }

    

    protected virtual void Hit()
    {
        Target.GetComponent<Monster>().TakeDamage(damage);
        Effect();
        Destroy(gameObject);        
    }

    public virtual void Effect(){}

}
