﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : Bullet
{
    public float SplashRad;

    protected override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
    }

    protected override void Hit()
    {
        Debug.Log("Splash");
        //base.Hit();
        List<GameObject> Splash = new List<GameObject>();
        Collider[] col = Physics.OverlapSphere(transform.position, SplashRad);
        foreach(Collider c in col)
        {
            if (c.tag == "Enemy")
            {
                Splash.Add(c.gameObject);
            }
        }   
        
        foreach(GameObject s in Splash)
        {
            s.GetComponent<Monster>().TakeDamage(damage);
        }

        Debug.Log(Splash.Count);
    }
    
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, SplashRad);
    }
}
