﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ice : Bullet {

    public float SlowSpeed;

    protected override void Start()
    {
        base.Start();   
    }

    protected override void Hit()
    {        
        Debug.Log("slow");
        Target.GetComponent<MonsterMovement>().SlowDown(SlowSpeed);
        base.Hit();
    }
}
