﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : Bullet
{
    public float Duration;

    protected override void Start()
    {
        base.Start();
    }

    protected override void Hit()
    {        
        base.Hit();
        Target.GetComponent<Monster>().Burn(Duration);       
    }
}
