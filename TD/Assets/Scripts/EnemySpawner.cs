﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Linq;

public class EnemySpawner : MonoBehaviour {

    public GameObject Flying, Ground, Boss;

    public int MaxWaves;
    public int ItemsPerWave;

    public Text WaveCounter;

    public GameObject SpawnLoc;

    GameObject[] monsters;
    int WaveNum;
    bool HasMonsters;

    private List<GameObject> spawns = new List<GameObject>();

    // Use this for initialization
    void Start () {
        WaveNum = 1;
        HasMonsters = false;
        
    }
	
	// Update is called once per frame
	void Update () {

        int WaveTemp = WaveNum - 1;
        if (WaveTemp > MaxWaves)
        {
            Application.Quit();
        }
       
        CheckMonster();
        if (HasMonsters == false)
        {
            SpawnWave();
        }

        WaveCounter.text = "Wave - " + WaveTemp.ToString();
    }

    void SpawnWave()
    {
        if (WaveNum % 2 == 0 && WaveNum % 5 != 0)
        {            
            for (int i = 0; i < ItemsPerWave; i++)
            {
                GameObject spawn = Instantiate(Ground, SpawnLoc.transform.position, Quaternion.identity);
                spawns.Add(spawn);
            }
        }

        if (WaveNum % 2 == 1 && WaveNum % 5 != 0)
        {            
            for (int i = 0; i < ItemsPerWave; i++)
            {
                Instantiate(Flying, SpawnLoc.transform.position, Quaternion.identity);
            }
        }

        if (WaveNum % 5 == 0)
        {           
            Instantiate(Boss, SpawnLoc.transform.position, Quaternion.identity);
        }
        Debug.Log("SpawnWave: " + WaveNum);
        WaveNum++;
    }

    void CheckMonster()
    {
        monsters = GameObject.FindGameObjectsWithTag("Enemy");

        if (monsters.Length <= 0)
        {
            HasMonsters = false;
            //Debug.Log("No monsters!");
        }

        else
        {
            HasMonsters = true;
            //Debug.Log("Monsters!");
        }

        spawns.RemoveAll(s => s == null);

    }
}
