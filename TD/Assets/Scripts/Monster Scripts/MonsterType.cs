﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterType : MonoBehaviour {
    public enum EnemType
    {
        Ground,
        Flying,
        Boss
    }
    public EnemType MyType;

}
