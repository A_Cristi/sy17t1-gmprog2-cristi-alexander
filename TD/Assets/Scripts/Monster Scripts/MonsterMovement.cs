﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterMovement : MonoBehaviour {

    public GameObject end;

    private Monster monster;

    float timeCounter = 0.0f;
    float TimetoSlow = 2.0f;
    bool isSlowed;
   
    UnityEngine.AI.NavMeshAgent nav;
    // Use this for initialization
    void Start () {
        monster = GetComponent<Monster>();
        isSlowed = false;
        end = GameObject.Find("Goal");

        nav = GetComponent<UnityEngine.AI.NavMeshAgent>();
        nav.destination = end.transform.position;
    }
	
	// Update is called once per frame
	void Update () {

        if(!isSlowed)
            nav.speed = monster.Data.Speed;

        CheckDes();
        timeCounter += Time.deltaTime;

    }

    void CheckDes()
    {
        if (gameObject.transform.position.x >= end.transform.position.x)
        {
            end.GetComponent<Goal>().HP--;
            Debug.Log("Died");
            Destroy(gameObject);
        }   
    }

    public void SlowDown(float speed)
    {
        isSlowed = true;
        timeCounter = 0.0f;
        Debug.Log("Slowed!");
        if (timeCounter < TimetoSlow)
        {
            if(isSlowed)
                nav.speed = speed;
        }
        else
        {
            isSlowed = false;
        }

    }
}
