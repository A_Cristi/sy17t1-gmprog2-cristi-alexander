﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnemyType
{
    Ground,
    Flying,
    Boss
}

[CreateAssetMenu(menuName = "Monster Data")]
public class MonsterData : ScriptableObject {

    public EnemyType Type;
    public float Health;
    public float Speed;
    public int GoldReward;

}
