﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Monster : MonoBehaviour {

    public MonsterData Data;
    float timer;

    public float Health, MaxHealth;

    bool isBurning;
    bool startBurn;
    void Start()
    {
        Health = Data.Health;
        MaxHealth = Health;
        isBurning = false;
        startBurn = false;
    }

    void Update()
    {
        if (Health <= 0)
        {
            GoldReward();
            Destroy(gameObject);
        }

        if (!isBurning && startBurn)
        {
            isBurning = true;            
            timer -= Time.deltaTime;
            Debug.Log(Health);
            if (timer >= 0)
            {
                TakeDamage(1);
                isBurning = false;
            }
        }
    }

    public void TakeDamage(float dmg)
    {
        Health -= dmg;
    }

    public void Burn(float dura)
    {
        timer = dura;
        startBurn = true;
    }

    void GoldReward()
    {
        GoldHolder GH = GameObject.Find("GoldHolder").GetComponent<GoldHolder>();
        GH.gold += Data.GoldReward;
    }
}
