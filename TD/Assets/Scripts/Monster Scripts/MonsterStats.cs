﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterStats : MonoBehaviour {

    public int Health;
    public float Speed;

    int level;

	// Use this for initialization
	void Start () {
        MonsterData data = GetComponent<Monster>().Data;
        Health = (int)data.Health;
        Speed = data.Speed;
	}
	
	// Update is called once per frame
	void Update () {
		if (Health <= 0)
        {
            Destroy(gameObject);
        }
	}

    public void TakeDamage(int dmg)
    {
        Health -= dmg;
    }
}
