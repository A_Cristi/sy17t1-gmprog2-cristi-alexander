﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Skill : MonoBehaviour {
    public float Timer;
    public bool Cooldown;

    float displayTimer;
    public Text timerText;

    protected void Start()
    {
        displayTimer = Timer;
        Cooldown = false;
    }

    protected virtual void Update()
    {
        timerText.text = displayTimer.ToString();

        if(Cooldown)
        {
            displayTimer -= Time.deltaTime;
        }
    }

    public virtual void Activated()
    {
        if (Cooldown)
        {
            Debug.Log("CD");
            return;
        }
        else if (!Cooldown)
        {
            
            Cooldown = true;
            Effect();
            Debug.Log("Skill used.");
            Invoke("CD", Timer);
        }
    }

    public virtual void Effect() { }

    public virtual void CD()
    {
        displayTimer = Timer;
        Cooldown = false;
    }
}
