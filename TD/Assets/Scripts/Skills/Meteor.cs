﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor : Skill {

    public int AOE;

    protected override void Update()
    {
        base.Update();
        Effect();
    }

    public override void Effect()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Collider[] colliders = Physics.OverlapSphere(Input.mousePosition, AOE);
            foreach (Collider collider in colliders)
            {
                if (collider.tag == "Enemy")
                {
                    Monster enemy = collider.GetComponent<Monster>();
                    enemy.Health *= .15f;
                }
            }
        }
    }

}
