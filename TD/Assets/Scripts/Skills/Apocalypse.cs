﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Apocalypse : Skill {

    private GameObject[] Enemies;

    public override void Effect()
    {
        //base.Effect();

        Enemies = GameObject.FindGameObjectsWithTag("Enemy");
        Debug.Log("Apocalypse");
        List<GameObject> RegEnemies = new List<GameObject>();

        foreach (GameObject enem in Enemies)
        {
            switch (enem.GetComponent<MonsterType>().MyType)
            {
                case MonsterType.EnemType.Ground:
                    RegEnemies.Add(enem);
                    break;

                case MonsterType.EnemType.Flying:
                    goto case MonsterType.EnemType.Ground;

                case MonsterType.EnemType.Boss:
                    break;
            }
        }

        foreach (GameObject enem in RegEnemies)
        {
            enem.GetComponent<Monster>().TakeDamage(99999);
        }

    }
}
