﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reaper : Skill {

    GameObject[] enemies;
    protected override void Update()
    {
        base.Update();

        Activated();
    }

    public override void Effect()
    {
        enemies = GameObject.FindGameObjectsWithTag("Enemy");
        foreach(GameObject enem in enemies)
        {
            if ((enem.GetComponent<Monster>().Health / enem.GetComponent<Monster>().MaxHealth) <= .15f)
            {
                Collider[] col = Physics.OverlapSphere(enem.gameObject.transform.position, 2);
                foreach (Collider ga in col)
                {
                    if (ga.tag == "Monster")
                    {
                        ga.GetComponent<Monster>().Health -= (enem.GetComponent<Monster>().Health / enem.GetComponent<Monster>().MaxHealth) * 100;
                    }
                }
                Destroy(enem);
            }

        }
    }
}
