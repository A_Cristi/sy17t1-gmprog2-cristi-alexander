﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerFire : MonoBehaviour {

    GameObject target;

    GameObject bulletPrefab;
	// Use this for initialization
	void Start () {
        bulletPrefab = gameObject.GetComponent<TowerBase>().BulletPrefab;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void GetTarget(GameObject g)
    {
        target = g;        
    }    

    public void Fire()
    {        
        GameObject bullet = Instantiate(bulletPrefab, transform.position, transform.rotation) as GameObject;

        bullet.GetComponent<Bullet>().Target = target;       
             
    }
}
