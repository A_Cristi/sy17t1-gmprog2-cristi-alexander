﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class TowerAI : MonoBehaviour { 

    public float TowerRange;

    public bool CanTargetFlying;
    public float FireRate;    

    GameObject target;
    TowerFire TF;

    List<GameObject> InRange = new List<GameObject>();

    // Use this for initialization
    void Start () {
        TF = gameObject.GetComponent<TowerFire>();
        InvokeRepeating("CheckEnemiesInRange", 0f, FireRate);        
	}
	
	// Update is called once per frame
	void Update () {
        if (target != null)
        {
            transform.LookAt(target.transform.position);
        }
    }

    void CheckEnemiesInRange()
    {         
        Collider[] inside = Physics.OverlapSphere(transform.position, TowerRange);
      
        for (int i = 0; i < inside.Length; i++)
        {
            if (inside[i].tag == "Enemy")
            {
                switch (inside[i].GetComponent<MonsterType>().MyType)
                {
                    case MonsterType.EnemType.Flying:
                        if (CanTargetFlying)
                            InRange.Add(inside[i].gameObject);
                        break;

                    case MonsterType.EnemType.Ground:
                        InRange.Add(inside[i].gameObject);
                        break;

                    case MonsterType.EnemType.Boss:
                        goto case MonsterType.EnemType.Ground;

                }

            }
        }        

        if (!InRange.Any()) return;
        if (InRange.Count == 0) return;

        //GameObject nearest = null;


        //float minSqrDistance = Mathf.Infinity;

        //for (int i = 0; i < InRange.Count; i++)
        //{
        //    float sqrDistanceToCenter = (transform.position - InRange[i].transform.position).sqrMagnitude;

        //    if (sqrDistanceToCenter < minSqrDistance)
        //    {
        //        minSqrDistance = sqrDistanceToCenter;
        //        nearest = InRange[i];
        //    }
        //}
        //target = nearest;

        target = InRange.OrderBy(g => Vector3.Distance(transform.position, g.transform.position)).FirstOrDefault();
        TF.GetTarget(target);
        TF.Fire();
        Debug.Log(InRange.Count);

        InRange.Clear();
    }    
        
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, TowerRange);
    }

    public GameObject AcquireTarget()
    {
        Collider[] inside = Physics.OverlapSphere(transform.position, TowerRange);

        for (int i = 0; i < inside.Length; i++)
        {
            if (inside[i].tag == "Enemy")
            {
                InRange.Add(inside[i].gameObject);
            }
        }
        Debug.Log(InRange.Count);

        return InRange.OrderBy(g => Vector3.Distance(transform.position, g.transform.position)).FirstOrDefault();

    }
}
