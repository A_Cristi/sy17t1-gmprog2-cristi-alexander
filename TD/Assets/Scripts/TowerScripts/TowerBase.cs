﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowerBase : MonoBehaviour {

    public GameObject BulletPrefab;

    public int twrCost;
    public int TowerUpgradeCost;

    public int TowerLvl;

    public Text TowerLvlText;
    public Text TowerCostToLvlText; 

    TowerAI TAI;
    public enum TowerType
    {
        Crossbow,
        Ice,
        Fire,
        Cannon
    }
    public TowerType MyTwrType;

    private void Start()
    {
        TAI = GetComponent<TowerAI>();
       
        TowerLvlText = GameObject.Find("TowerLvl").GetComponent<Text>();
        TowerCostToLvlText = GameObject.Find("TowerCost").GetComponent<Text>();
    }

    public virtual void Fire()
    {
        // Get target
        GameObject target = GetComponent<TowerAI>().AcquireTarget();        
    }

    public void Upgrade()
    {
        if (TowerLvl <= 3)
        {
            TowerLvl++;
            TAI.FireRate -= .3f;
        }        
    }

    private void OnMouseDown()
    {
        TowerLvlText.text = "Level - " + TowerLvl.ToString();
        TowerCostToLvlText.text = "Upgrade cost -- " + TowerUpgradeCost.ToString();
    }
    private void OnMouseOver()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Upgrade();
        }
    }

}
