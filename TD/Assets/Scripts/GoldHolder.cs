﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GoldHolder : MonoBehaviour {

    public Text GoldText;
    public int gold;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        GoldText.text = "Gold - " + gold;

	}
}
